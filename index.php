<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="img/campfire_icon.png" />
        <title>Lifestyle Store</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
       
        <link rel="stylesheet" href="bootstrap.min.css" type="text/css">
        
        <script type="text/javascript" src="jquery-3.2.1.min.js"></script>
        
        <script type="text/javascript" src="bootstrap.min.js"></script>
        
        <link rel="stylesheet" href="style.css" type="text/css">

        <link href="https://fonts.googleapis.com/css?family=Gelasio&display=swap" rel="stylesheet">
    </head>
    <body>
        <div>
           <?php
            require 'header.php';
           ?>
           <div id="bannerImage">
               <div class="container">
                   <center>
                   <div id="bannerContent">
                       <h1>We sell expert and novice camping gear.</h1>
                       <p>Create Your Own Adventure</p>
                       <a href="products.php" class="btn btn-danger">Shop Now</a>
                   </div>
                   </center>
               </div>
           </div>
           <div class="container">
               <div class="row">
                   <div class="col-xs-4">
                       <div  class="thumbnail">
                           <a href="products.php">
                                <img src="img/Eclipse Dome Tent.png" alt="Tents">
                           </a>
                           <center>
                                <div class="caption">
                                        <p id="autoResize">Tents</p>
                                        <p>Choose among the best available in the world.</p>
                                </div>
                           </center>
                       </div>
                   </div>
                   <div class="col-xs-4">
                       <div class="thumbnail">
                           <a href="products.php">
                               <img src="img/Kids' Ace 50 Overnight Backpack.png" alt="Backpacks">
                           </a>
                           <center>
                                <div class="caption">
                                    <p id="autoResize">Backpacks</p>
                                    <p>Original backpacks from the best brands.</p>
                                </div>
                           </center>
                       </div>
                   </div>
                   <div class="col-xs-4">
                       <div class="thumbnail">
                           <a href="products.php">
                               <img src="img/Mountain Trapper 20 Degree F Sleeping Bag.png" alt="Sleeping bag">
                           </a>
                           <center>
                               <div class="caption">
                                   <p id="autoResize">Sleeping Bags</p>
                                   <p>Our exquisite collection of sleeping bags.</p>
                               </div>
                           </center>
                       </div>
                   </div>
               </div>
           </div>
            <br><br> <br><br><br><br>
           <footer class="footer"> 
               <div class="container">
               <center>
               <p>Copyright &copy Carl's Camping Corner. All Rights Reserved. </p>
                   <p>This website designed by students at North Georgia Technical College</p>
               </center>
               </div>
           </footer>
        </div>
    </body>
</html>